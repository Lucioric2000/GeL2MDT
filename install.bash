#git clone https://github.com/Ensembl/ensembl-vep.git
sudo yum install perl git unzip cpan wget gcc bzip2 python-devel

#Declare the location of the conda installaction
condabin=`which conda`
if [ -z $condabin ]
then
    conda_home=/opt/conda
    #Install the Miniconda Python pachages manager
    echo "Next, the Miniconda package will be downloaded and installed"
    echo "You should install it at the default location shown"
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    sh Miniconda3-latest-Linux-x86_64.sh -p $conda_home
    rm Miniconda3-latest-Linux-x86_64.sh
    #Make the updated shell path available in this session:
    source ~/.bashrc
else
    conda_home=${condabin%/bin/conda}
    echo "Conda installation found at $conda_home. Script will use tht installation."
fi
echo -e "\n#Shell environment for qiagen\nexport LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:${srv_qiagen}/bin/ssw/src/"|cat ~/.bashrc ->~/.bashrc.new
#Move the file ~/.bashrc.new to ~/.bashrc (overwriting the existent ~/.bashrc withouk asking for confirmation)
mv -f ~/.bashrc.new ~/.bashrc

git clone https://github.com/Lucioric2000/GeL2MDT
cd GeL2MDT
cd ensembl-vep
sudo cpan install DBI
sudo cpan install CPAN
perl INSTALL.pl
cd ..
conda create -n gel2mdt python=3.6
source activate gel2mdt
conda install virtualenv
pip install --upgrade pip
virtualenv .
source bin/activate
pip install -r requirements.txt
cd gelweb/
#Then you should enter to mysql with a username with administrative rights, and create a database with the command:
#CREATE DATABASE gel2mdt_db;
#Optionally, a user can be created with the command:
#create USER Jag_M;

python manage.py migrate
python manage.py makemigrations gel2mdt
python manage.py migrate
python manage.py createinitialrevisions

